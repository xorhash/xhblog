.TL
Thoughts on self-styling a hardware security module
.AU
16 June 2019
.AB no
.AE
.LP
I've been wanting a hardware security module (HSM) for a while now.
My use case mainly covers a local certificate authority that spans all
my local devices and virtual machines,
storing my TOTP tokens more securely,
and supplying (part of) various encryption keys,
such as my main machine's hard drive encryption key
and my password manager passphrase.
When thinking about it, I realized that an HSM meeting my needs has to
satisfy only three criteria:
.IP a.
it is network-attached but physically separate,
.IP b.
the software stack is minimal and designed for security, and
.IP c.
the hardware is tamper-evident.
.LP
Point (a) is a tradeoff between security and convenience.
It would be more secure if it was fully physically separate,
but that is too inconvenient because of frequent certificate issuance
and closing/reopening my password manager.
.PP
Point (b) is actually a solved problem, much to my surprise.
I thought I would need at least some custom code,
but it seems I can just take OpenBSD and run Vault%%https://www.vaultproject.io/
on it.
Very ideally, I would have a privilege-separated daemon where the
network handling is a separate process from the secret storage and
have a microkernel running instead of a full *NIX kernel.
However, that would entail writing a lot of software on my own,
which will almost certainly end up having more issues than off-the-shelf
OpenBSD and Vault.
Vault supports TOTP and has a native CA mode.
The CA root certificate, however, will be stored separately and
encrypted elsewhere;
Vault only needs to steer the intermediate CA.
.PP
Point (c) is the biggest challenge for me since
I only have commodity hardware available.
A security bag would be an ideal way to do tamper-protection,
but cooling becomes an immediate issue,
as would talking to the outside world over the ethernet port.
I
.I think
I can get close enough with security tape over all the ports
that are not occupied as well as all the screws.
The machine can operate in a headless fashion,
but this leaves the ethernet cable unaccounted for.
An ethernet cable can be visually inspected.
I'm not sure how to work around a cable swap for a cable that has
an ASIC and non-volatile storage to store the transferred data
somewhere inside itself.
However, this point becomes moot to me:
If I am dealing with an attacker with
.I that
amount of sophistication to employ ASICs,
I probably have bigger issues and really should go out and buy an
actual HSM already.
Maintenance will be another challenge there:
If I ever need to open up the machine,
I'll have to tamper with it and thus tear off most of the security
tape,
making it hard to distinguish between the time I tore it off and
an attacker.
In that case, I'd probably have to trash the machine entirely.
Fortunately, security tape seems to be available at fairly cheap
prices, so it is at least financially reasonable,
as long as I have cheap spare hardware as well.
.PP
I'm not yet sure which way I'll go (or if I'll seriously start this
undertaking at all),
but I think the project is feasible if I do want to go down that
rabbit hole.
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
