.TL
Designing an Integer Result Type
.AU
11 March 2019
.AB no
.AE
.NH
Overview of error handling in C
.LP
C notoriously has little in the ways of error handling.
There are no exceptions.
The only ways to meaningfully convey that a function has failed are
(a)\~returning a
.B NULL
pointer,
(b)\~returning some kind of integer error code,
(c)\~returning some kind of stringified error (and make
.B NULL
look like success),
(d)\~returning a boolean to indicate success, or
(e)\~shutting down the entire program.
.PP
Option\~(a) is only a useful way to communicate failure when the
function returns a pointer.
.PP
Options\~(b) and\~(c) are the ones found most commonly.
The UNIX API generally assumes that a return code of 0 indicates
success,
non-zero normally indicates failure;
the failure return codes are normally negative.
The cause is then saved in
.B errno .
A similar concept can be found in libgcrypt.
Windows also makes use of this pattern where an
.B HRESULT
is returned from a function.
Option\~(c) is an esoteric variant of option\~(b) that I don't think
I've ever seen in the wild,
but must surely exist, given the infinite wisdom of infinite monkeys
hacking on a keyboard.
.PP
Option\~(d) can be found very commonly on Windows, too.
It seems kind of like a variant of UNIX error handling,
using booleans over the return value being 0,
as the error information has to be queried separately from the return
value anyway.
.PP
Usually, you won't find option\~(e) in the wild since it is the most
irrecoverable form of error recovery.
Option\~(e) is used intentionally can normally be found when there is no
meaningful way to continue, e.g. on memory allocation failure.
LibreSSL also makes use of this exit strategy when getting random
numbers from the system fails.
.PP
Of particular interest here is option\~(b) in the style of
.B HRESULT .
I will call these \(lqresult types\(rq from here on out.
.NH
Examples of result types
.LP
Result types are hardly new,
but there's certainly still room to perfect it.
Let's first take a look at Microsoft's
.B HRESULT :
It reserves 5\~bits at the top for meta information,
10\~bits for the \(lqfacility\(rq and 
16\~bits for the actual error
code.%%https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-erref/0642cb2f-2075-4469-918c-4441e69c548a
The lowest bit (S\~bit) indicates failure if set (i.e., 0x80000000
indicates failure).
The bit after that (R\~bit) is used for interoperability with another
result type,
.B NTSTATUS .
The next bit (C\~bit) is set if the result is a \(lqcustomer-defined\(rq
one;
I assume this means that applications are free to use
.B HRESULT
internally as long as they set the C\~bit.
The next bit (N\~bit) is set if the type is to be reinterpreted as a
.B NTSTATUS .
And the fifth reserved bit (X\~bit) is reserved for future usage.
The \(lqfacility\(rq indicates the general source of the error,
such as RPC, Wininet, Windows Defender.
Finally, the actual error code is free-form,
but probably should be zero on success so that 0 always indicates
success.
.PP
Nintendo's Horizon operating system also has a result type
called
.B Result .
Its definition differs between the 3DS and the Switch, however.
On the 3DS, the Result type is a signed 32-bit
integer.%%https://smealum.github.io/ctrulib/types_8h.html#ac830bf5a4f2cf8273f61ab99a46cf163
Values greater than or equal to zero indicate success;
negative values indicate failure.%%https://smealum.github.io/ctrulib/result_8h.html
Apart from the sign bit, the top 4\~bits are used to indicate the
\(lqlevel\(rq of the Result.%%https://www.3dbrew.org/wiki/Error_codes
The level indicates the severity of the Result,
such as it being success/informational, being a temporary error (think
EAGAIN), API usage error or a fatal error.
A 5-bit \(lqsummary\(rq follows,
which is menat to categorize the error, like \(lqwould block\(rq or
\(lqout of resources\(rq.
7\~bits then indicate the module (of the operating system),
which is more or less equivalent to the \(lqfacility\(rq on Windows.
Finally, 10 bits are available for the actual error code,
which modules/facilities can use as they wish.
Because not every error indicates failure \(en only negative ones do
\(en, it is possible to communicate special circumstances through
positive non-zero Results.
.PP
On the Switch, the Result type has been simplified and shortened.
It is now a 22-bit integer;
it is assumed this has been done to make the error codes fit into an
AArch64/ARM64 MOV immediate.%%https://switchbrew.org/wiki/Error_codes
The sign bit is no longer being made use
of.%%https://github.com/switchbrew/libnx/blob/567828f44ee046167793d6572edd1e937a4d96ed/nx/include/switch/types.h#L47
The 12\~higher bits are available for the description,
the lower 10\~bits are used for the module/facility.
.NH
Designing a new result type
.LP
Some ideas are worthwhile, others seem to be somewhat circumstantial.
.B
Using a bit to indicate success or failure is useful;
.R
it becomes possible to indicate non-failure conditions through the error
code.
This is useful in cases where the happy path and the failure path
diverge, but special action must be taken on the happy path.
Consider a kernel facility that provides synchronous inter-process
communication.
It can use a non-failure error code internally to indicate that it must
wait for the other process to handle the message.
Alternatively, consider an HTTP library:
It can reserve the error code on success to instead indicate the content
length.
Both Microsoft and Nintendo deal with special constraints.
Microsoft has to reserve some bits for interoperability with the
NTSTATUS type;
Nintendo on the Switch has to make error codes fit into an AArch64 MOV
immediate.
Neither of these are constraints I'll have to deal with.
.B
The error summary used by the 3DS is unhelpful.
.R
At best, it's confusing, at worst, it makes developers look in the wrong
place.
Having an error code table available to developers is indispensable;
the summary bits are an insufficient debugging aid.
So let's do away with it.
Microsoft's NTSTATUS inspired something else, however:
A user-space result type should have a way to wrap operating system
errors (as in,
.I errno ).
.I errno
is normally a small integer value, which a result type could
transparently forward.
This, together with a module indicator in the result type could help
investigation.
.PP
So let's take a look at what I came up with and then analyze where it
came from.
I've settled on the following design for a 32-bit unsigned integer:
.TS
tab(|), box, center;
lfB lfB
l l.
Bit(s)|Meaning
_
31|Is failure?
30|Is fatal?
29|Is wrapping errno?
28-21|Module/Component
20-4|Description
3-0|Checksum
.TE
.LP
It is a 32-bit integer again:
It provides a decent amount of space while also fitting naturally in a
register of an 32-bit or 64-bit x86 machine.
Since negative integers are usually difficult to handle and harder to
parse in your head,
it is an unsigned 32-bit integer.
The failure bit at the top is something I consider useful as stated
above.
Instead of a severity indication, however, there is a second bit to
indicate an error being fatal, i.e. it is irrecoverable in the current
session/program, such as invalid credentials.
The errno wrapping bit just indicates if errno is being wrapped;
the other bits keep the application's context.
Something every result type had, the module/component field, was also
kept.
This aids the separation of concerns even inside a program with multiple
components.
Finally, a full 17\~bits are available for the description/actual error
code.
This has been done intentionally to be able to pass somewhat meaningful
information on non-failure.
A checksum follows at the very end,
though its usage is not required.
The checksum allows (a)\~clearly distinguishing result codes from other
integers while debugging, and (b)\~verifying that an error code provided
by a user is not typoed or made up.
.PP
I am pretty happy with the type as it is.
It's proven fairly versatile.
An example of its use can be found in my nsd CTF/ARG
code.%%https://gitlab.com/xorhash/nsd/blob/master/NewSysDump/result.h
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
.\" vim: set tw=72 sw=4 ts=4 syntax=groff:
