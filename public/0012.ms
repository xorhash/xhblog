.TL
Amazingly, writing a tail -f on Windows is a hard problem
.AU
28 March 2020
.AB no
.AE
.NH
The problem domain
.LP
Many of you may be familiar with the *NIX utility tail(1).
If given the -f flag, it keeps trying to read the file to keep reading
new lines if available.
The earliest implementations%%https://minnie.tuhs.org//cgi-bin/utree.pl?file=4.3BSD-Reno/src/usr.bin/tail/tail.c
would just sleep for a second,
then re-try to read from the current position.
In 4.4BSD, tail(1) was changed to instead use select(2) to sleep for one
second,%%https://minnie.tuhs.org//cgi-bin/utree.pl?file=4.4BSD/usr/src/usr.bin/tail/forward.c
which it does as an optimization because sleep(3) was actually several
system calls.
.PP
The modern-day OpenBSD implementation of tail -f, however, make their
life easier:
They use kevent(2), which gives them notifications for when there is
more data to be read.
The same mechanism can be used to determine if a file was truncated.\**
.FS
Now, some evidently particularly sadistic person also decided that
ftruncate(2) can truncate to an
.I arbitrary
offset.
This new size is not forwarded by OpenBSD kevent(2).
.FE
FreeBSD, in the meantime, doesn't detect truncation and instead hooks
into renames and moves, losing the straight truncation case.
I needed similar functionality on Windows as this snippet for OpenBSD
provides, which is mostly just the core of tail -f:
.DS
FILE *logfile;
char *line = NULL;
size_t linesz = 0;
ssize_t nr;
struct kevent ev;
int kq;

if ((logfile = fopen("some.log", "rt")) == NULL)
    err(1, "fopen some.log");

if ((kq = kqueue()) == -1)
    err(1, "kqueue");

EV_SET(&ev, fileno(logfile), EVFILT_READ, EV_ADD | EV_CLEAR,
        0, 0, NULL);
if (kevent(kq, &ev, 1, NULL, 0, NULL))
    err(1, "kevent adding events");
EV_SET(&ev, fileno(logfile), EVFILT_VNODE, EV_ADD | EV_CLEAR,
        NOTE_TRUNCATE, 0, NULL);
if (kevent(kq, &ev, 1, NULL, 0, NULL))
    err(1, "kevent adding events");

/*
 * No seek to the end since we assume we started before or soon after
 * writer.
 */

while (kevent(kq, NULL, 0, &ev, 1, NULL)) {
    /*
     * On truncation, we need to reset the file pointer.
     * We're guaranteed to get NOTE_TRUNCATE before EVFILT_READ.
     */
    if ((ev.fflags & NOTE_TRUNCATE) != 0) {
        warnx("truncated");
        fpurge(logfile);
        fseek(logfile, 0, SEEK_SET);
    } else { /* must be the read event then */
        /* read until EOF */
        while ((nr = getline(&line, &linesz, logfile)) != -1)
            fputs(line, stdout);
        if (ferror(logfile))
            errx(1, "ferror logfile");
        clearerr(logfile); /* reset buffer EOF flag */
    }
} 

close(kq);
fclose(logfile);
.DE
.LP
Surely, on Windows, this couldn't be too hard to accomplish, right?
I was trying to do this in C#, mind you, but as it turns out, it's
turtles all the way down and the underlying WinAPI function suffers the
same issue.
As a design decision, Windows prefers I/O completion ports (IOCP) over
polling-style/event-style programming like with kevent(2).
Therefore, there is also no way to check if there is new stuff to read
in a file other than by taking the 2.9BSD approach:
Sleep, try to read, repeat.
No kernel events for you...
almost.
.PP
Windows does let you monitor events on a directory, which you can then
filter down to an individual file:
C#/.NET System.IO.FileSystemWatcher,%%https://docs.microsoft.com/en-us/dotnet/api/system.io.filesystemwatcher?view=netframework-4.8
which is based on WinAPI ReadDirectoryChangesW().%%https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-readdirectorychangesw?redirectedfrom=MSDN
However, the result is not reliable;
it may miss events or batch multiple events into a single one, causing
delays of several seconds upwards.%%https://social.msdn.microsoft.com/Forums/vstudio/en-US/4465cafb-f4ed-434f-89d8-c85ced6ffaa8/filesystemwatcher-reliability?forum=netfxbcl
I had an issue that needed relatively real-time responses,
not several seconds of delays,
so I ultimately settled on 2.9BSD-style sleep, read, repeat, with a
sleep time of 10\~milliseconds between each attempt.
Thanks, Windows.
I love you, too.
.NH
License for this text
.LP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
