.TL
Thoughts on Digital Privacy in 2019
.AU
21 February 2019
.AB no
.AE
.LP
It's February 21, 2019.
I open The Register and what do I see on the front page?
A Twitter account getting
compromised,%%https://www.theregister.co.uk/2019/02/21/77_brigade_twitter_account_hacked/
rumors of a data breach in the UK Labour
Party,%%https://www.theregister.co.uk/2019/02/21/data_breach_labour_locks_down_member_databases/
a 14-year-old security vulnerability in
WinRAR,%%https://www.theregister.co.uk/2019/02/20/winrar_security_bug/
advertising giants likely breaking the EU General Data Protection
Regulation (GDPR)
intentionally%%https://www.theregister.co.uk/2019/02/20/iab_rtb_complain_fresh_evidence/
and more arguing about Huawei maybe adding spying equipment to their
chips.%%https://www.theregister.co.uk/2019/02/19/germany_huawei_5g_security/
This is a horrendous state of affairs.
.PP
In light of that, I believe we need to ask ourselves a question:
.B
Should we abandon privacy?
.R
What we have right now is a large body of corporations based in the U.S.
whose main or even only purpose is to gather and aggregate personal
data,
ranging from religious beliefs over pregnancy to what you likely had for
dinner yesterday.
Looking at the EU's efforts with the GDPR,
Europe seems to be carving a niche for itself with privacy.
But it might not be tackling a worthwhile problem.
It's clear that the advertisers have no interest whatsoever in privacy.
The advertising industry literally makes hundreds of billions of
U.S.\~dollars.%%https://web.archive.org/web/20151001222501/http://www.carat.com/au/en/news-views/carat-predicts-positive-outlook-in-2016-with-global-growth-of-plus47/
There's no way this big money will just allow it to go away because the
EU is throwing a temper tantrum.
.PP
Rather than trying to stop the inevitable like the GDPR is attempting,
.B
We should be using the private sector's data for the public good.
.R
Law enforcement has a notoriously difficult time,
leading to the Investigatory Powers Act 2016 in the
U.K.%%https://en.wikipedia.org/wiki/Investigatory_Powers_Act_2016
Meanwhile, Google and others may be sitting on location and
communications data that could allow much quicker.
Rather than having to request data from the corporations,
it would be vastly more efficient to push the data to every nation's law
enforcement agencies, or at least giving them frictionless read access.
.PP
Anthropology studies could greatly benefit from statistics made from
aggregated data.
Google, Facebook and Amazon are sitting on so much data that could be
combined in virtually endless, fascinating ways.
It seems like a waste to just let it rot away.
.PP
Oversharing becomes less of a problem if
.I everyone
does it.
There would just be too much to sift through.
Additionally, this has the benefit of deterring Joe Average from saying
inappropriate things or challenging authorities.
Destabilizing events like the gillets jaunes in France could not even
flare up if social media were more actively scanned and automatically
alerted the authorities.
Facebook has implemented an automated version of suicide
watch.%%https://techcrunch.com/2017/11/27/facebook-ai-suicide-prevention/
Lives can literally be saved like this.
.PP
We live in a post-privacy society.
People are reluctant to accept that, me included.
Legislation is required to finally bring the online world on par with
the real world, in a coordinated effort from the United Nations:
.IP \(en
Every citizen of every nations requires a digital passport.
The digital passport is checked by every online service.
.IP \(en
Protocols other than HTTP are firewalled on an ISP level,
everywhere.
Enthusiasts and businesses can still use non-standard ports and non-HTTP
services within their LAN.
This limits the amount of evasion of the new system.
.IP \(en
Public VPN providers are to be systematically harrassed to join the
post-privacy movement and sell out their customers,
or banned outright.
Businesses may still operate VPNs to allow access to their intranet.
.LP
The true culmination of crime prevention are thought scanners,
which identify individuals liable to fall to the dark side and those who
are planning to or already have committed a crime.
However, it seems that such a thing has not yet been invented.
I look forward to the day it will have been.
.PP
.B
This entire article may be satire.
Please get angry at your own discretion.
.R
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
.\" vim: set tw=72 sw=4 ts=4 syntax=groff:
