.TL
A List of Things I Keep Searching
.AU
9 March 2019
.AB no
This is just a list of websites, commands, etc. that I keep searching
for.
So now I'm writing them down here so that I have to put less effort into
searching.
This list will inevitably expand over time.
.AE
.IP \(en
.B
Punch holes into a file (make sparse file; Linux-only):
.R
.br
$ fallocate -d <file>
.IP \(en
.B
POSIX/SUS standard:
.R
.br
http://pubs.opengroup.org/onlinepubs/9699919799/%%http://pubs.opengroup.org/onlinepubs/9699919799/
.IP \(en
.B
UNIX Text Processing book:
.R
.br
http://home.windstream.net/kollar/utp/%%http://home.windstream.net/kollar/utp/
.IP \(en
.B
Joke about threading:
.R
.br
``Roses are red, violets are blue. now I have two. I fixed one threading
problem,''%%https://twitter.com/invalidname/status/963788391233064960
.IP \(en
.B
AIX mapping NULL:
.R
.br
https://groups.google.com/d/msg/comp.unix.aix/0dv4N0qmgVQ/tyhnSiOLD0cJ%%https://groups.google.com/d/msg/comp.unix.aix/0dv4N0qmgVQ/tyhnSiOLD0cJ
.IP \(en
.B
Caldera license validity:
.R
.br
https://virtuallyfun.com/wordpress/2018/11/26/\&why-bsd-os-is-the-best-candidate-for-being-the-only-tested-legally-open-unix/%%https://virtuallyfun.com/wordpress/2018/11/26/why-bsd-os-is-the-best-candidate-for-being-the-only-tested-legally-open-unix/
.IP \(en
.B
Google's patent on blocking via unsolvable CAPTCHA:
.R
.br
Patent US9407661B2%%https://patents.google.com/patent/US9407661
