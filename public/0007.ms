.TL
*NIX Structs
.AU
2 May 2019
.AB no
.AE
.LP
I keep needing these structs because other things reference them,
but it's hard to remember where to find their definition.
So let's just put all of them here.
All man page references are OpenBSD-specific;
it seems every OS documents their socket structs somewhere else.
.DS L
struct tm /* ctime(2) */ {
    int tm_sec;    /* Seconds (0-60) */
    int tm_min;    /* Minutes (0-59) */
    int tm_hour;   /* Hours (0-23) */
    int tm_mday;   /* Day of the month (1-31) */
    int tm_mon;    /* Month (0-11) */
    int tm_year;   /* Year - 1900 */
    int tm_wday;   /* Day of the week (0-6, Sunday = 0) */
    int tm_yday;   /* Day in the year (0-365, 1 Jan = 0) */
    int tm_isdst;  /* Daylight saving time */
};
.DE
.DS L
struct stat /* stat(2) */ {
    dev_t      st_dev;    /* inode's device */
    ino_t      st_ino;    /* inode's number */
    mode_t     st_mode;   /* inode protection mode */
    nlink_t    st_nlink;  /* number of hard links */
    uid_t      st_uid;    /* user ID of the file's owner */
    gid_t      st_gid;    /* group ID of the file's group */
    dev_t      st_rdev;   /* device type */
    struct timespec st_atim;  /* time of last access */
    struct timespec st_mtim;  /* time of last data modification */
    struct timespec st_ctim;  /* time of last file status change */
    off_t      st_size;   /* file size, in bytes */
    blkcnt_t   st_blocks; /* blocks allocated for file */
    blksize_t  st_blksize;/* optimal blocksize for I/O */
    /* everything past this is non-standard, see stat(2) */
};
.DE
.DS L
struct timespec /* clock_gettime(2) */ {
    time_t  tv_sec;       /* seconds */
    long    tv_nsec;      /* and nanoseconds */
};
.DE
.DS L
struct sockaddr /* netintro(4) */ {
    u_int8_t        sa_len;         /* total length */
    sa_family_t     sa_family;      /* address family */
    char            sa_data[14];    /* actually longer */
};
.DE
.DS L
struct sockaddr_in /* inet(4), cast to struct sockaddr */ {
    u_int8_t        sin_len;
    sa_family_t     sin_family;
    in_port_t       sin_port;
    struct in_addr  sin_addr; /* get via inet_pton(3) */
    int8_t          sin_zero[8];
};
.DE
.DS L
struct sockaddr_in6 /* inet6(4), cast to struct sockaddr */ {
    u_int8_t        sin6_len;
    sa_family_t     sin6_family;
    in_port_t       sin6_port;
    u_int32_t       sin6_flowinfo;
    struct in6_addr sin6_addr; /* get via inet_pton(3) */
    u_int32_t       sin6_scope_id;
};
.DE
.DS L
struct sockaddr_un /* unix(4), cast to struct sockaddr */ {
    u_char  sun_len;
    u_char  sun_family;
    char    sun_path[104];
};
.DE
