.TL
Hitting the Hacker News Front Page: Numbers for a Nascent Repository
.AU
21 April 2019
.AB no
.AE
.NH
What happened
.LP
While studying up on how OpenBSD's implementation of RCS works,
I came across diff(1)%%https://man.openbsd.org/diff.1
having an option
.B -e ,
which outputs a script for for ed(1).
Then I wondered how long that has been available.
As it turns out, it's been there since at least Seventh Edition
UNIX.%%https://man.openbsd.org/UNIX-7/diff.1
The man page there also contained an interesting remark:
.QP
The
.B -e
option produces a script of
.I a ,
.I c
and
.I d
commands for the editor
.I ed ,
which will recreate
.I file2
from
.I file1 .
[...].
In connection with
.B -e ,
the following shell program may help maintain multiple versions
of a file.
Only an ancestral file ($1) and a chain of version-to-version
.I ed
scripts ($2,$3,...) made by
.I diff
need be on hand.
A `latest version' appears on the standard output.
.DS
(shift; cat $*; echo ´1,$p´) ⎪ ed - $1
.DE
.LP
I figured ``Well, surely even I can make this.''
So I made it,%%https://github.com/xorhash/fh
with a couple more bells and whistles added so as not to
be too sad an excuse for a version control system.
In an afternoon.
It was supposed to be a not very funny joke.
.NH
Hacker News
.LP
Having finished my work in one afternoon,
I ended up submitting it to Lobsters and Hacker News.
It hit the front page on Hacker News after about two hours and
earned 109\~points in 17\~hours.
Interestingly, it only garnered eight\~comments,
suggesting that either nobody took a closer look at it yet
or it was indeed taken as the joke I'd intended it to be.
.PP
As it turns out, GitHub offers a feature called
.I Insights .
After the aforementioned 17\~hours passed,
I looked at the insights page:
.IP \(en
the repository got 1470\~views (1144\~unique visitors) from Hacker News;
.IP \(en
the
.I man.md
compilation of man pages has only been viewed 106\~times,
so less than 10\~% of people actually checked the manual;
.IP \(en
the actual code in the
.I fl ,
.I fr
and
.I fu
scripts have been viewed (total) 199\~times;
.IP \(en
the repository got 66\~stars;
.IP \(en
the repository has only been cloned four\~times.
.LP
It seems interesting how fickle interest can be.
While there has been a non-negligible amount of traffic
to the repository,
the amoutn of people that took any kind of genuine interest
is just around 10\~% of clicks.
I can only imagine how frustrating this must be if a start-up's product
hits the HN front page and hardly anything happens.
.NH
Conclusion
.LP
For a joke, the repository has run a surprisingly long course.
I do take pull requests and issues,
but I strongly encourage people to use
RCS or SCCS if they need single-file version control.
Incidentally, I do believe the interleaved deltas in SCCS are the
most elegant design,
even if the algorithms to read and manipulate it are difficult to grasp.
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
