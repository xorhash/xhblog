.TL
Why I think people are mad about systemd
.AU
8 July 2019
.AB no
.AE
.LP
Recently, a number of technologies specifically started on Linux have
been generating controversy.
Many will be familiar with systemd, which is the target of some intense
controversy.%%https://en.wikipedia.org/wiki/Systemd#Criticism
Similarly, now that Wayland is ships with Debian stable by
default,%%https://www.debian.org/News/2019/20190706
and the apparent X.org maintainers have announced that X.org will be
going into hard maintenance mode
soon,%%https://blogs.gnome.org/uraeus/2019/06/24/on-the-road-to-fedora-workstation-31/
which caused a number of people to be fairly disappointed on Hacker
News.%%https://news.ycombinator.com/item?id=20376041
Oftentimes, technical arguments will be brought up,
but I don't think the technical arguments reflect why people are mad.
.PP
Instead, it seems to me that
.B
people are mad because parts of the system change that they expected not
to change.
.R
In essence, they believe their legitimate expectations of system
component stability have been disappointed.
In *NIX, it is normal for things to change over time, too.
However, these changes are rarely drastic changes of the foundations of
the system:
Nobody expects that mv(1) is without replacement and people are told to
just use cp(1) and rm(1) instead.\**
.FS
Though in case of the recent Wayland debate,
the luxury of having a viable 1:1 alternative does not exist in many
scenarios.
I'm not sure how I'll proceed with my WM-only Openbox setup;
Openbox has been unmaintained for four
years,%%https://github.com/danakj/openbox
so it seems unreasonable to expect Wayland support to come.
Simlarly, I've got family members on XFCE,
which has seen its last release four years ago,
and has no Wayland
plans.%%https://mail.xfce.org/pipermail/xfce4-dev/2018-April/032116.html
.FE
However, for some reason people thought that X.org and a traditional
init(8) would be constituents of the ``fossilized'' parts.
As it turns out, people can and will rewrite things that are thought to
be stable, fossilized components.
.PP
In face of all the kicking and screaming about these changes, people
don't seem to be leaving Linux for other *NIX operating systems.
I could imagine this being caused by it being
.I infeasible
to leave.
In a sense, Linux has become the Windows of the server space because
everything assumes Linux.
.IP \(bu
Rust has tier\~1 support for Windows, macOS and Linux
only.%%https://forge.rust-lang.org/platform-support.html
.IP \(bu
\&.NET Core runs on Windows, macOS and Linux
exclusively.%%https://github.com/dotnet/core/blob/master/release-notes/3.0/3.0-supported-os.md
.IP \(bu
Wayland is Linux-first, everybody else
second.%%https://www.phoronix.com/scan.php?page=news_item&px=Wayland-BSD-Improving-2019
.IP \(bu
systemd is by design a Linux project,
and GNOME keeps integrating more tightly with
it.%%https://lists.debian.org/debian-devel/2013/10/msg00578.html
.IP \(bu
etc.
.LP
This is exactly the same vendor lock-in that people fled Windows for,
but were okay with because ``it's open source'':
No, people won't maintain things for you forever,
you're exactly as trapped as you would be on Windows
and you're just now slowly waking up to it.
Things will change in breaking ways,
and you have
.I no
say in it whatsoever.
At least the licensing fees are cheaper I guess.
.PP
It also appears to me that the Linux community may have to start
scrutinizing Red Hat as a corporation the way they scrutinize Apple and
Microsoft.
Undoubtedly, Red Hat has done much for the open source ecosystem on
Linux.
However, because of their enormous weight in the desktop system,
they can push changes through major Linux systems by virtue of making
changes to GNOME.%%https://lists.debian.org/debian-devel/2013/10/msg00578.html
GNOME has won the desktop war:
Debian, Ubuntu and Red Hat Enterprise Linux all ship GNOME as the
default desktop with seemingly no intention to change;
Red Hat even removed support for KDE from
version\~8.%%https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/7.6_release_notes/index#chap-Red_Hat_Enterprise_Linux-7.6_Release_Notes-Deprecated_Functionality
.PP
This invokes the, admittedly rather interesting, thought of possibly the
GNU userland itself being replaced on Linux.
Given these recent trends, I can absolutely people to try and create a new
userland, while also changing parts and existing paradigms in breaking
ways.
This is both an interesting and terrifying thought because it fully
cements the Linux ecosystem as not just another *NIX, but an operating
system sui generis that just so happens to be mostly POSIX compatible
not by design, but rather only where it seems convenient.
If this comes to pass, these changes
.I will
come from the Linux universe;
the BSDs and illumos don't seem to have too much interest in upending
system foundations like that.
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
