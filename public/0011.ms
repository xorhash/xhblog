.TL
Thoughts on Daniel J. Bernstein's document IDs
.AU
14 February 2020
.AB no
.AE
.NH
What are document IDs?
.LP
I'm using the term \(lqdocument ID\(rq to refer to the 16-byte hex strings
that Daniel J. Bernstein (djb) embeds in footnotes in of all of his
papers.
For example, the ChaCha paper has an ID of
4027b5256e17b9796842e6d0f68b0b5e.%%https://cr.yp.to/chacha/chacha-20080128.pdf
.NH
Why have document IDs?
.LP
djb claims to use document IDs to enable people to find a paper,
even if the URL changes from which it can be
retrieved.%%https://cr.yp.to/bib/documentid.html
A search for the string \(lqPermanent ID of this document:\(rq followed by
the hash should find a paper if it is available on the Internet.
The same concept applies for closed systems in fields of study that generally
prohibit authors to put papers on the Internet;
in that case, the same method works for the proprietary search engines of the
publishers involved.
.PP
It also makes it possible to find papers that
.I cite
a certain paper,
so long as they (a) actually use the document ID,
and (b) do not reuse that particular phrase in the citation.
This breaks in scenarios in which papers are not published to the Internet
\(en or at least are not indexed by major search engines
(which is not necessarily mutually exclusive with paywalls);
in that case, the papers just cannot be found easily.
.NH
How are djb's document IDs determined?
.LP
djb generates the IDs by taking the head(1) of /dev/urandom and
generating the MD5 hash of that.
I'm not sure why he goes through the trouble of doing this.
/dev/urandom already yields randomness that is sufficient for cryptographic
purposes;%%https://www.2uo.de/myths-about-urandom/
perhaps it's just that
.DS
head /dev/urandom | md5sum
.DE
is easier to remember and type than
.DS
dd if=/dev/urandom bs=16 count=1 | xxd -p
.DE
is.
There is no correlation between the document for which the ID is generated
and the actual ID.
.PP
Incidentally, this is one of the few cases for which MD5 is still okay to use:
You only care about the randomness of the output given an ephemeral input,
there is nothing to attack in the first place.
.NH
Issues with the approach as practiced
.LP
I agree that document IDs are helpful to locate documents that have gone away
and to identify mirrors of documents;
djb himself rarely breaks links, so he's actually not even making good use of
it, so to speak.
However, there are still a few issues I find with the way things work right now:
.PP
Searchability depends on using a particular phrase and knowing that
you need to enter it along with the ID to locate the original;
this is suboptimal for everyone who is uninitiated.
djb may be a personality that's important enough for people to be aware of his
conventions,
but this hardly applies to everyone.
.PP
A document is identified by the combination of ID and date.
Revisions of the document get a new date, but keep their ID.
It impairs searchability:
By searching for an ID, you may accidentally find an older version.
While I'm sure djb doesn't make any major adjustments to existing documents,
other people using the system might not have all that much foresight and would
perhaps make major changes, yet keep the ID regardless,
making it difficult to find the most current version.
Nor does this seem intuitive to me:
Looking at web forums and blogs,
I inherently expect an \(lqID\(rq to refer to something that is malleable, yes;
but papers are not web forums and blogs.
Documents intended for dead trees feel more like they should be stable \(en
even if only ever their digital version is published as a PDF and they're never
printed.
In that sense, I expect a document ID to refer to a specific version of a
specific document.
.PP
For some reason, djb does not use this system for his website.
Considering that he's taken down parts of it in response to a
lawsuit,%%https://cr.yp.to/crypto.html
perhaps the searchability argument is just as relevant to websites that
may have been mirrored as it is to actual papers.
Furthermore, some websites may contain very substantial information;
take for example extensive answers on Stack Overflow.
.PP
Finally, and djb himself also notes this,
there is no way to protect against malice.
There is no relation of any kind between the author, the document and its ID.
djb notes that \(lqone solution is to limit the search to documents signed under
the author's public key (which is, presumably, included in citations),
but current search engines don't support this feature\(rq.
There is no reason to throw the baby out with the bathwater here:
Some best efforts improvements can be made,
even if no sufficient security can be offered.
.NH
What I may be doing going forward
.LP
What follows is what I've got in mind for my own organization.
I haven't settled on any of this,
so this is still fairly idle speculation on improvements;
especially as I do not yet feel confident enough to burden people with the
confusion that comes out from having indecipherable strings on pages.
.PP
The searchability issue can be addressed by having
.I two
IDs:
one for the document itself,
and one to be used when citing it.
Observant users of document IDs will use the second one in their bibliography.
The sloppy ones may still use the first one,
but that would also imply that they are unable to read,
and as such the effort that's gone into their research can be called into
question as a whole.
.PP
The failure to uniquely identify a document by ID alone and requiring the
date can also be fixed easily:
Insert a new ID for each revision.
This could even be automated by integrating it into the workflow with a version
control system so that every revision also has an ID (even if never published).
.PP
Document IDs can be linked to the author and the document in at least a
rudimentary form:
by signing a string that encompasses the title in a canonicalized form
and the current revision.
There's still the possibility of taking a signed document ID and applying
it to another document,
but at least people can find out that there are two conflicting IDs and realize
that something may be amiss.
.PP
Assume the document is stored in git and a PDF was built at git commit
524cd7f66f88b86a9892b7b7d81ca8d7a8587299 and shows this title:
.DS C
A cryptanalysis of ChaCha6
How little crypto is too little?
.DE
The main title would be \(lqA cryptanalysis of ChaCha6\(rq,
to be encoded in UTF-8.
This could be followed by the git commit ID, and finally followed by
either a static \(lqID\(rq or \(lqcite\(rq depending on which ID it is.
.PP
Signatures, however, are very
.I bulky .
An EdDSA signature over Curve25519 weighs in at 64\~bytes;
even with base\~64 encoding,
this comes out to 86\~characters without padding for one signature.
Assuming a reasonable line length at 10.5\~pt text size (Fira Sans)
and a sheet of A4,\**
.FS
A reasonable line length is somewhere between 45\~and 90\~characters,
see Butterick's Practical Typography.
My personal favorite is in the range of 70\(en78\~characters though.
.FE
this does not even fit a normal line,
much less terse \(lqDocument ID:\(rq and \(lqTo cite:\(rq annotations
for the IDs themselves.
Footnote sizes are usually smaller, but even reducing the size down to 8\~pt
for the ID footnote in particular,
it's a very tight fit.
Any less than 8\~pt would be way too strenuous to read on paper in particular.
EdDSA as-is therefore cannot be an option.
I have some hope for BLS signatures,
which yield 48-byte signatures
(64\~characters in base\~64, which fits comfortably at 8\~pt even with a
monospace font and tagging the IDs for their purpose)
for a 128-bit security level;%%https://tools.ietf.org/html/draft-irtf-cfrg-bls-signature-00
however, implementations are still very few,
partly because the implementation complexity is very high.\**
.FS
Implementations
.I do
exist.
However, for reasons that boil down to my own irrational preferences,
I am not very interested in the current ones:
The C reference implementation is licensed under the
Apache\~2.0 license,%%https://github.com/kwantam/bls12-381_hash
which is in conflict with the OpenBSD copyright
policy;%%https://www.openbsd.org/policy.html
I do very much agree with the OpenBSD copyright policy and would like to
avoid software failing to meet its standards.
For the same reason, I've been disregarding Chia Network's C++
implementation.%%https://github.com/Chia-Network/bls-signatures
The implementation by herumi%%https://github.com/herumi/bls
in C++ \(en which isn't C,
but at least it's a language I could put up with \(en
and is licensed under the 3-clause BSD license
has been really uncomfortable to build due to consisting of multiple
subprojects,
so I'd rather something more convenient come along like TweetNaCl.
While already here, I'd like to express my great displeasure about the
lack of cryptographic bignum libraries with liberal licensing terms.
.FE
Perhaps there are other practical signature schemes that I've overlooked,
which I would be very happy to find out about.
One may also wonder if a 128-bit security level really is appropriate here,
considering that these signatures are going to be as long lived as the dead
trees that the text may be printed on.
However, quantum cryptography threatens to throw out everything anyway,
so I feel like there
.PP
A theoretical alternative would be to use a keyed hash which could be truncated
to more amenable sizes,
but that means nobody other than you can ever actually verify document IDs:
Because the key is symmetric,
you cannot give it out for people to verify because then they can forge IDs.
.PP
Even if I were to assume a suitable signature scheme exists and generates
48-byte signatures or smaller,
the issue of encoding persists.
Base\~64 can produce offensive output;
a string could, for example, contain \(lqfuck\(rq in varying capitalizations;
those unfamiliar with base\~64 encoding would thus reasonably be offended,
especially if they spot offensive words near the start or ending of the IDs.
It is thus necessary to replace at least AEIOUYaeiouy,
perhaps substituted with !#$%&*-_.:;? or some other series of ASCII
interpunction.
.NH
License for this text
.LP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
