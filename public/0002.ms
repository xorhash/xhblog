.TL
DRM on Software is a Necessary Evil
.AU
21 February 2019
.AB no
.AE
.NH
What is DRM on Software?
.LP
\(lqDRM\(rq stands for \(lqdigital rights management\(rq.
For the purpose of this post,
In this post, DRM refers to technical measures in software
that restrict or expand the available features
and that takes some kind of data supplied by the vendor in order to do
so.
It can do operate alone or in combination with hardware or other
software.
The goal of DRM is to make available the functionality of the software
only to customers who have paid for it.
DRM comes in many forms, such as software that requires:
.IP a.
serial numbers and product keys (e.g. Microsoft Windows, mIRC),
.IP b.
activation servers (e.g. Microsoft Windows, Steam),
.IP c.
dedicated hardware (e.g. video game consoles such as Nintendo\~Switch),
.IP d.
encryption keys (e.g. Steam, Nintendo\~Switch eShop),
.IP e.
local-network scans for duplicate installations (e.g. Xinuos UnixWare).
.LP
The measures above can be mixed and matched for even more fine-tuning.
.NH
Why is DRM evil?
.LP
DRM turns the relationship between the user of the software and the
vendor on its head.
By the implementation of DRM,
the vendor acknowledges that there may be people who use the software
without properly licensing it,
incriminating the user by default and placing the burden of proof for a
legitimate purchase on the user.
.PP
The default state of software where the DRM parameters are not available
is to be unusable.
If the DRM parameters become unavailable \(en perhaps because the vendor
has taken the activation servers
down%%https://helpx.adobe.com/creative-suite/kb/creative-suite-2-activation-end-life.html
or has gone out of business \(en,
so does the software.
.PP
Software whose vendors paid insufficient amount of attention to the
deployment process at scale also tends to be hard to roll out in any
kind of medium to large organization:
For example, a certain piece of software may only be sold with unique
product keys and the product keys must be manually entered during
installation.
It then becomes difficult to automate installation of said software
across workstations.
.NH
Why is DRM necessary?
.LP
Despite the complaints above, I consider DRM to be necessary.
For one, unauthorized copying of software is an undeniable
phenomenon.%%https://globalstudy.bsa.org/2011/downloads/study_pdf/2011_BSA_Piracy_Study-InBrief.pdf
DRM sends a message to people using software without proper licenses.
For purely moral reasons, I believe that commercial software should be
kept out of the hands of those who have not paid for them.
Those who claim not to be able to afford the software must either
use alternatives or pay up.
There is no middle ground.
The poor do not magically gain rights to software.
.PP
Furthermore, the reality of life as a software vendor is that there is
an expectation that you not only provide support for your software,
but also maintain it, optimize it and continue developing features.
To do so, you as a software vendor require an income stream.
By way of DRM (namely activation servers), you can cut off software that
has outlived its profitability of a sale.
This forces new sales after a while,
allowing software vendors to stay in business.
The epitome of this is software that is not sold or permanently
licensed,
but rather available purely on a subscription basis.
.PP
Users may also just accidentally violate licenses.
For example, they may have a volume license and only a single key that
has an activation limit for as many copies as they are licensed to use.
DRM can help prevent a state of unlicensed usage.
This relieves small and medium businesses from having to perform
invasive software audits on their enterprise customers' premises.
.PP
DRM may also be the easiest way to forge complex licensing agreements.
In particular in the business-to-business space,
software licensing can become hideously complex.
Shifting the complex and multi-layered legal stipulations into a piece
of software makes compliance for the customer easier than having one
more legal detail to keep track of,
even though it has no measurable impact on its day-to-day operations.
.PP
Finally, DRM provides motivation for some people to get started in the
space of reverse engineering and information security.
An aspiring hacker%%http://www.catb.org/esr/faqs/hacker-howto.html
may find it to be a worthwhile exercise to break
through existing DRM because they have a problem with the DRM for
whatever reason,
i.e. an acute, pressing need to acquire the skills necessary.
.PP
To the greatest extent possible under applicable law,
I have waived all copyright and related or neighboring rights to this
blog post under the CC0 1.0 Universal Public Domain Dedication;
see for details:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
.\" vim: set tw=72 sw=4 ts=4 syntax=groff:
