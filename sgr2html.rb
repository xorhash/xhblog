#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (c) 2019 xorhash
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

ESC = "\x1b"

$status = []

def reset
  if !$status.empty?
    # close in reverse order of opening: <b><i> => </i></b>
    $status.reverse_each do |s|
      case s
      when :bold
        $stdout.write('</b>')
      when :italic
        $stdout.write('</i>')
      when :underline
        $stdout.write('</u>')
      end
    end
  end
  $status = []
end

while ch = $stdin.getc
  if ch == ESC
    if $stdin.getc != '['
      $stderr.puts("unescaped character 0x#{n.ord.to_s(16)} after ESC") 
      exit(1)
    end

    # CSI confirmed, read until 'm'
    s = ''
    loop do
      ch = $stdin.getc
      break if ch == 'm'
      s += ch
    end

    # simplification: assume we never see ;
    case s
    when '0'
      reset()
    when ''
      reset()
    when '22'
      $status -= [:bold]
      $stdout.write('</b>')
    when '23'
      $status -= [:italic]
      $stdout.write('</i>')
    when '24'
      $status -= [:underline]
      $stdout.write('</u>')
    when '1'
      $status << :bold
      $stdout.write('<b>')
    when '3'
      $status << :italic
      $stdout.write('<i>')
    when '4'
      $status << :underline
      $stdout.write('<u>')
    else
      $stderr.puts("unhandled SGR sequence #{s}")
    end

    next
  end

  $stdout.putc(ch)
end

