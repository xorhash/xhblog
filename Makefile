.SUFFIXES: .ms .html

SRCS = public/0001.html \
	   public/0002.html \
	   public/0003.html \
	   public/0004.html \
	   public/0005.html \
	   public/0006.html \
	   public/0007.html \
	   public/0008.html \
	   public/0009.html \
	   public/0010.html \
	   public/0011.html \
	   public/0012.html

.ms.html:
	./growrap.rb $< | ./sgr2html.rb | cat header.html - footer.html > $@
	./title.sh $< $@

all: public/index.html

public/index.html: $(SRCS)
	cat header.html > public/index.html
	sed -i 's/{{TITLE}}/xhblog index/' public/index.html
	for i in public/[0-9][0-9][0-9][0-9].html; do\
		printf -- '- <a href="%s">{{TITLE}}</a>\n' $$(basename $$i) >> public/index.html;\
		./title.sh $${i%%.html}.ms public/index.html;\
	done
	cat footer.html >> public/index.html

